﻿using System.Collections.Generic;

namespace CollectorService.DataProviders
{
    interface IDataProvider
    {
        IEnumerable<string> GetData();
    }
}
