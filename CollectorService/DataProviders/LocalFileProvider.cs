﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CollectorService.DataProviders
{
    class LocalFileProvider : IDataProvider
    {
        private readonly string filepath;
        long lastPosition = 0;

        public LocalFileProvider(string filepath)
        {
            if (!File.Exists(filepath))
            {
                throw new ArgumentException($"File '{filepath}' does not exist.", nameof(filepath));
            }

            this.filepath = filepath;
        }

        public IEnumerable<string> GetData()
        {
            using (Stream stream = File.Open(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                stream.Seek(lastPosition, SeekOrigin.Begin);
                using (var streamReader = new StreamReader(stream))
                {
                    while (!streamReader.EndOfStream)
                    {
                        yield return streamReader.ReadLine();
                    }
                    lastPosition = stream.Position;
                }
            }
        }
    }
}
