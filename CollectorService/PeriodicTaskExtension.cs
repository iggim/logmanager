﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CollectorService
{
    public class PeriodicTask
    {
        public static Task Run(Action action, TimeSpan period, CancellationToken cancellationToken)
        {
            Action wrapperAction = async () =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (!cancellationToken.IsCancellationRequested)
                    {
                        action();
                    }

                    await Task.Delay(period, cancellationToken);
                }
            };

            return Task.Factory.StartNew(wrapperAction, cancellationToken);
        }

        public static Task Run(Action action, TimeSpan period)
        {
            return Run(action, period, CancellationToken.None);
        }

    }
}
