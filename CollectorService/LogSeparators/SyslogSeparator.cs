﻿using System.Collections.Generic;

namespace CollectorService.LogSeparators
{
    class SyslogSeparator : ILogSeparator
    {
        public IEnumerable<string> Separate(IEnumerable<string> lines)
        {
            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    // sometimes logs are separated with an empty line
                    continue;
                }

                yield return line;
            }
        }
    }
}
