﻿using System.Collections.Generic;

namespace CollectorService.LogSeparators
{
    interface ILogSeparator
    {
        IEnumerable<string> Separate(IEnumerable<string> lines);
    }
}
