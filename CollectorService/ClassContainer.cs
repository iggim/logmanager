﻿using Microsoft.Practices.Unity.Configuration;
using System.Configuration;
using Unity;

namespace CollectorService
{
    public class ClassContainer
    {
        private static readonly ClassContainer ClassContainerInstance = new ClassContainer();

        public static ClassContainer Instance
        {
            get
            {
                return ClassContainerInstance;
            }
        }

        public IUnityContainer UnityContainer { get; private set; }

        private ClassContainer()
        {
            this.UnityContainer = new UnityContainer();
            UnityConfigurationSection configurationSection = ConfigurationManager.GetSection("unity") as UnityConfigurationSection;
            if (configurationSection == null)
            {
                return;
            }
            configurationSection.Configure(this.UnityContainer);
        }
    }
}
