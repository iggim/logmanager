﻿using CollectorService.LogProviders;
using LogManager.Base.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace CollectorService
{
    class Worker
    {
        private readonly EventLog eventLog;
        private readonly ILogProvider[] logProviders;
        private readonly IMessageQueueHelper messageQueue;

        private CancellationTokenSource tokenSource;

        private List<Task> tasks = new List<Task>();

        public Worker(EventLog eventLog, ILogProvider[] logProviders, IMessageQueueHelper messageQueue)
        {
            this.eventLog = eventLog;
            this.logProviders = logProviders;
            this.messageQueue = messageQueue;
        }

        public bool Start()
        {
            tokenSource = new CancellationTokenSource();
            var cancellation = tokenSource.Token;

            foreach (var logProvider in logProviders)
            {
                tasks.Add(
                    PeriodicTask.Run(
                        () => FetchLogs(logProvider),
                        TimeSpan.FromSeconds(60),
                        cancellation)
                );
            }

            return true;
        }

        public bool Stop()
        {
            tokenSource.Cancel();
            var timeout = TimeSpan.FromSeconds(60);

            return Task.WaitAll(tasks.ToArray(), timeout);
        }

        private void FetchLogs(ILogProvider logProvider)
        {
            try
            {
                foreach (var log in logProvider.Fetch())
                {
                    messageQueue.AddMessage(log);
                }
            }
            catch (Exception e)
            {
                eventLog.WriteEntry(
                    $"Failed to fetch logs from source{logProvider.Source}.{Environment.NewLine}"
                    + $"Error message: {e.Message}{Environment.NewLine}Trace: {e.StackTrace}");
            }
        }

    }
}
