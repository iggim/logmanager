﻿using LogManager.Base.Models;
using System.Collections.Generic;

namespace CollectorService.LogProviders
{
    interface ILogProvider
    {
        string Source { get; }
        IEnumerable<LogData> Fetch();
    }
}
