﻿using CollectorService.DataProviders;
using CollectorService.LogSeparators;
using LogManager.Base.Models;
using System.Collections.Generic;

namespace CollectorService.LogProviders
{
    class DefaultLogProvider : ILogProvider
    {
        private readonly LogLevel level;
        private readonly IDataProvider dataProvider;
        private readonly ILogSeparator logSeparator;

        public string Source { get; private set; }

        public DefaultLogProvider(string source, LogLevel level, IDataProvider dataProvider, ILogSeparator logSeparator)
            : this(source, dataProvider, logSeparator)
        {
            this.level = level;
        }

        public DefaultLogProvider(string source, IDataProvider dataProvider, ILogSeparator logSeparator)
        {
            this.level = LogLevel.Info;
            this.Source = source;
            this.dataProvider = dataProvider;
            this.logSeparator = logSeparator;
        }

        public IEnumerable<LogData> Fetch()
        {
            var lines = dataProvider.GetData();

            foreach (var log in logSeparator.Separate(lines))
            {
                yield return new LogData
                {
                    Source = Source,
                    Level = level,
                    RawLog = log
                };
            }

        }
    }
}
