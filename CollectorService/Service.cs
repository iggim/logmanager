﻿using System.Diagnostics;
using System.ServiceProcess;
using Unity;

namespace CollectorService
{
    public partial class Service : ServiceBase
    {
        private Worker worker;
        protected IUnityContainer UnityContainer => ClassContainer.Instance.UnityContainer;

        public Service()
        {
            InitializeComponent();

            SetupEventLog();

            worker = UnityContainer.Resolve<Worker>();
        }

        private void SetupEventLog()
        {
            if (!EventLog.SourceExists("CollectorService"))
            {
                EventLog.CreateEventSource("CollectorService", "LogManager");
            }
            eventLog.Source = "CollectorService";
            eventLog.Log = "LogManager";

            // Make it available through unity container
            UnityContainer.RegisterInstance(eventLog);
        }

        protected override void OnStart(string[] args)
        {
            eventLog.WriteEntry("Starting service!", EventLogEntryType.Information);

            bool startedSuccessfully = false;
            try
            {
                startedSuccessfully = worker.Start();
            }
            finally
            {
                if (startedSuccessfully)
                {
                    eventLog.WriteEntry("Service started!", EventLogEntryType.Information);
                }
                else
                {
                    // Task didn't complete during reasonable amount of time
                    eventLog.WriteEntry("Failed to start service!", EventLogEntryType.Error);
                }
            }
        }

        protected override void OnStop()
        {
            bool finishedSuccessfully = false;
            try
            {
                finishedSuccessfully = worker.Stop();
            }
            finally
            {
                if (finishedSuccessfully)
                {
                    eventLog.WriteEntry("Service stoped successfully!", EventLogEntryType.Information);
                }
                else
                {
                    // Task didn't complete during reasonable amount of time
                    eventLog.WriteEntry("Failed to gracefully shut down service!", EventLogEntryType.Error);
                }
            }
        }
    }
}
