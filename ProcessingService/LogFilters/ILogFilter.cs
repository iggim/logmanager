﻿using LogManager.Base.Models;
using System.Threading.Tasks;

namespace ProcessingService.LogFilters
{
    interface ILogFilter
    {
        Task<bool> ShouldKeepAsync(NormalizedLog log);
    }
}
