﻿using LogManager.Base.Models;
using System.Threading.Tasks;

namespace ProcessingService.LogFilters
{
    class LogLevelFilter : ILogFilter
    {
        private readonly LogLevel level;

        public LogLevelFilter(LogLevel level)
        {
            this.level = level;
        }

        public async Task<bool> ShouldKeepAsync(NormalizedLog log)
        {
            return log.Level >= level;
        }
    }
}
