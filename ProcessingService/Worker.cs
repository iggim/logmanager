﻿using LogManager.Base.Models;
using ProcessingService.Stages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace ProcessingService
{
    /// <summary>
    /// https://stackoverflow.com/questions/27383996/basic-design-pattern-for-using-tpl-inside-windows-service-for-c-sharp
    /// </summary>
    internal class Worker
    {
        protected IUnityContainer Container => ClassContainer.Instance.UnityContainer;

        private CancellationTokenSource tokenSource;
        private List<Task> tasks = new List<Task>();

        private BlockingCollection<NormalizedLog> parsedLogs;
        private BlockingCollection<NormalizedLog> filteredLogs;
        private BlockingCollection<NormalizedLog> augmentedLogs;

        public Worker()
        {
            parsedLogs = new BlockingCollection<NormalizedLog>();
            filteredLogs = new BlockingCollection<NormalizedLog>();
            augmentedLogs = new BlockingCollection<NormalizedLog>();
        }

        public bool Start()
        {
            tokenSource = new CancellationTokenSource();
            var cancellation = tokenSource.Token;

            ParseStage parseStage = Container.Resolve<ParseStage>();
            //parseStage.Cancelled += (object sender, EventArgs e) => Task.WaitAll(tasks.ToArray());

            Task.Factory.StartNew(() => parseStage.DoWorkAsync(null, parsedLogs, cancellation),
                cancellation,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);

            //FilterStage filterStage = Container.Resolve<FilterStage>();
            //Task.Factory.StartNew(() => filterStage.DoWorkAsync(parsedLogs, filteredLogs, cancellation),
            //    cancellation,
            //    TaskCreationOptions.LongRunning,
            //    TaskScheduler.Default);

            AugmentStage augmentStage = Container.Resolve<AugmentStage>();
            Task.Factory.StartNew(() => augmentStage.DoWorkAsync(parsedLogs, augmentedLogs, cancellation),
                cancellation,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);

            StoreStage storeStage = Container.Resolve<StoreStage>();
            Task.Factory.StartNew(() => storeStage.DoWorkAsync(augmentedLogs, null, cancellation),
                cancellation,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);

            return true;
        }

        /// <summary>
        /// Stops tasks.
        /// </summary>
        /// <returns>True if tasks finished successfully, else false</returns>
        public bool Stop()
        {
            tokenSource.Cancel();
            var timeout = TimeSpan.FromSeconds(60);

            return Task.WaitAll(tasks.ToArray(), timeout);
        }

    }
}
