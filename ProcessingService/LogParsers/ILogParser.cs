﻿using LogManager.Base.Models;

namespace ProcessingService.LogParsers
{
    interface ILogParser
    {
        NormalizedLog Parse(LogData log);
    }
}
