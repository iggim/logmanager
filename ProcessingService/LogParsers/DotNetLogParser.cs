﻿using LogManager.Base.Models;
using Newtonsoft.Json;

namespace ProcessingService.LogParsers
{
    class DotNetLogParser : ILogParser
    {
        public NormalizedLog Parse(LogData log)
        {
            var deserializedLog = JsonConvert.DeserializeObject<DotNetLog>(log.RawLog);

            return new NormalizedLog(log)
            {
                User = deserializedLog.User,
                DateGenerated = deserializedLog.DateTime,
                Message = deserializedLog.Message,
                IpAddress = deserializedLog.IpAddress
            };
        }
    }
}
