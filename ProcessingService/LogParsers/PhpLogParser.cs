﻿using LogManager.Base.Models;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProcessingService.LogParsers
{
    class PhpLogParser : ILogParser
    {
        public PhpLogParser()
        {
        }

        private string[] Info = { "Notice" };
        private string[] Warning = { "Warning" };
        private string[] Error = { "Parse error", "Catchable fatal error", "Fatal error" };

        public NormalizedLog Parse(LogData log)
        {
            var normalizedLog = new NormalizedLog(log);

            // [12-Apr-2017 17:33:18 Europe/Berlin] PHP Fatal error:  Call to undefined function get_header() in C:\xampp\htdocs\test\kallyas\index.php on line 1

            Match match = Regex.Match(log.RawLog, @"^\[(.+?)\] PHP (.+?): (.+?)$");
            if (match.Success)
            {
                var time = match.Groups[1].Value;
                var level = match.Groups[2].Value;
                var message = match.Groups[3].Value;

                normalizedLog.DateGenerated = DateTime.Parse(time);
                normalizedLog.Message = message;

                if (Info.Contains(level, StringComparer.OrdinalIgnoreCase))
                {
                    normalizedLog.Level = LogLevel.Info;
                }
                else if (Warning.Contains(level, StringComparer.OrdinalIgnoreCase))
                {
                    normalizedLog.Level = LogLevel.Warning;
                }
                else
                {
                    normalizedLog.Level = LogLevel.Error;
                }
            }

            return normalizedLog;
        }

    }
}
