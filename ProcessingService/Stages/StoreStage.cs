﻿using LogManager.Base.Helpers;
using LogManager.Base.Models;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessingService.Stages
{
    class StoreStage : BaseStage
    {
        private readonly ILogStorageHelper logStorage;

        public StoreStage(EventLog eventLog, ILogStorageHelper logStorage)
            : base(eventLog)
        {
            this.logStorage = logStorage;
        }

        public override async Task DoWorkAsync(BlockingCollection<NormalizedLog> source, BlockingCollection<NormalizedLog> destination, CancellationToken token)
        {
            foreach (NormalizedLog log in source.GetConsumingEnumerable())
            {
                try
                {
                    // Persist log in permanent storage
                    await logStorage.StoreAsync(log);
                }
                catch (Exception e)
                {
                    LogMessage($"Failed to persist normalized log. Error message: {e.Message}");

                    // TODO: Retry? Save in local file so we don't lose log data.
                }
            }
        }
    }
}
