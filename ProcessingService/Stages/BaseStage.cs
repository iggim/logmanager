﻿using LogManager.Base.Models;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessingService.Stages
{
    abstract class BaseStage
    {
        protected EventLog eventLog;

        public BaseStage(EventLog eventLog)
        {
            this.eventLog = eventLog;
        }

        protected void LogMessage(string message)
        {
            if (eventLog != null)
            {
                eventLog.WriteEntry(message);
            }
        }

        public abstract Task DoWorkAsync(BlockingCollection<NormalizedLog> source, BlockingCollection<NormalizedLog> destination, CancellationToken token);
    }
}
