﻿using LogManager.Base.Helpers;
using LogManager.Base.Models;
using ProcessingService.LogParsers;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace ProcessingService.Stages
{
    class ParseStage : BaseStage
    {
        private readonly IMessageQueueHelper messageQueue;
        protected IUnityContainer Container => ClassContainer.Instance.UnityContainer;
        public event EventHandler Cancelled;

        public ParseStage(EventLog eventLog, IMessageQueueHelper messageQueue)
            : base(eventLog)
        {
            this.messageQueue = messageQueue;
        }

        public override async Task DoWorkAsync(BlockingCollection<NormalizedLog> source, BlockingCollection<NormalizedLog> destination, CancellationToken token)
        {
            while (true)
            {
                LogData log = await messageQueue.GetMessageAsync();

                ILogParser logParser = Container.Resolve<ILogParser>(log.Source);

                NormalizedLog normalizedLog = logParser.Parse(log);
                destination.Add(normalizedLog);

                if (token.IsCancellationRequested)
                {
                    Cancelled(this, new EventArgs());

                    // no more new tasks
                    break;
                }
            }
        }
    }
}
