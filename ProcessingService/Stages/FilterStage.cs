﻿using LogManager.Base.Models;
using ProcessingService.LogFilters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessingService.Stages
{
    class FilterStage : BaseStage
    {
        private readonly IEnumerable<ILogFilter> logFilters;

        public FilterStage(EventLog eventLog, ILogFilter[] logFilters)
            : base(eventLog)
        {
            this.logFilters = logFilters;
        }

        public override async Task DoWorkAsync(BlockingCollection<NormalizedLog> source, BlockingCollection<NormalizedLog> destination, CancellationToken token)
        {
            foreach (NormalizedLog log in source.GetConsumingEnumerable())
            {
                try
                {
                    foreach (var filter in logFilters)
                    {
                        if (!await filter.ShouldKeepAsync(log))
                        {
                            continue;
                        }
                    }
                }
                catch (Exception e)
                {
                    LogMessage($"Failed to filter log. Error message: {e.Message}");
                    // We are leaving log because it is better to have unimportant log than to discard important one.
                }

                destination.Add(log);
            }
        }
    }
}
