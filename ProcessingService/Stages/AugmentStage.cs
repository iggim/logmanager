﻿using LogManager.Base.Models;
using ProcessingService.LogAugmenters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessingService.Stages
{
    class AugmentStage : BaseStage
    {
        private readonly IEnumerable<ILogAugmenter> logAugmenters;

        public AugmentStage(EventLog eventLog, ILogAugmenter[] logAugmenters)
            : base(eventLog)
        {
            this.logAugmenters = logAugmenters;
        }

        public override async Task DoWorkAsync(BlockingCollection<NormalizedLog> source, BlockingCollection<NormalizedLog> destination, CancellationToken token)
        {
            foreach (NormalizedLog log in source.GetConsumingEnumerable())
            {
                NormalizedLog augmentedLog = log;
                foreach (ILogAugmenter augmentor in logAugmenters)
                {
                    try
                    {
                        augmentedLog = await augmentor.AugmentAsync(augmentedLog);
                    }
                    catch (Exception e)
                    {
                        // do not stop processing
                        // just log exception and continue in order to prevent log data loss
                        eventLog.WriteEntry(e.Message, EventLogEntryType.Error);

                        // TODO: log exception
                    }
                }

                destination.Add(augmentedLog);
            }
        }
    }
}
