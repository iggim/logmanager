﻿using LogManager.Base.Helpers;
using LogManager.Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace ProcessingService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
            culture.DateTimeFormat.LongTimePattern = "";
            Thread.CurrentThread.CurrentCulture = culture;
            Console.WriteLine(DateTime.Now);

            if (Environment.UserInteractive)
            {
                System.Console.WriteLine("press 'q' to quit.");

                //if (!EventLog.SourceExists("ProcessingService"))
                //{
                //    EventLog.CreateEventSource("ProcessingService", "LogManager");
                //}
                var eventLog = new EventLog
                {
                    Source = "ProcessingService",
                    Log = "LogManager"
                };

                // Make it available through unity container
                ClassContainer.Instance.UnityContainer.RegisterInstance(eventLog);


                var tokenSource = new CancellationTokenSource();
                var cancellation = tokenSource.Token;

                Task.Factory.StartNew(() => GenerateLogs(cancellation),
                    cancellation,
                    TaskCreationOptions.LongRunning,
                    TaskScheduler.Default);

                var app = new Worker();
                app.Start();
                while (Console.Read() != 'q')
                {
                }
                tokenSource.Cancel();
                app.Stop();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new Service()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        static readonly Random rand = new Random();

        private static void GenerateLogs(CancellationToken token)
        {
            var msmq = ClassContainer.Instance.UnityContainer.Resolve<IMessageQueueHelper>();
            string[] usernames = { "page.hildebrand", "izetta.richie", "mistie.trahan", "sherril.baer", "ines.cave", "odell.leblanc", "brianna.kramer", "an.sipes", "hettie.gaffney", "arica.bartley", "alysa.pack", "josefine.lennon", "angeles.dallas", "napoleon.farr", "alvina.allard", "caroyln.goulet", "ayako.farrow", "lindy.dukes", "savannah.newcomb", "isobel.denning", "mollie.grey", "darell.elias", "bethanie.ammons", "nidia.katz", "kaye.benavides", "merlin.prentice", "jarrett.tobin", "alejandrina.gunter", "lasonya.cheek", "gricelda.mott", "lyda.orlando", "christiana.wilder", "alessandra.gilson", "kira.gilmore", "angelyn.rosenbaum", "sheridan.shea", "chung.satterfield", "reed.person", "dion.reedy", "nakesha.weston", "felton.baumgartner", "leatha.hewitt", "loris.pease", "cayla.mesa", "jeanene.romano", "annett.fortner", "maryellen.fonseca", "pearlie.strand", "christen.dorman", "setsuko.mcmahan", "shala.savage", "alpha.galarza", "lonny.hagan", "hiram.hannah", "minh.barnhill", "natalia.forsyth", "thi.oliveira", "bobbie.steinberg", "isela.estes", "dyan.schindler", "jone.rush", "floy.broussard", "joane.danielson", "ivory.sperry", "lashaunda.cope", "maryland.landry", "piper.benoit", "nanette.zuniga", "refugia.bankston", "dennis.duckett", "kai.whitfield", "vanita.parson", "felipa.stauffer", "donny.kuykendall", "brice.sousa", "clemente.maynard", "denny.nation", "lara.kincaid", "pierre.sierra", "evangeline.martins", "vern.neeley", "mavis.dale", "akilah.couch", "nikole.hathaway", "mercedez.reilly", "tamar.herrmann", "frankie.childs", "kathline.driver", "delilah.ybarra", "senaida.ham", "desirae.oliva", "damion.duckworth", "carrol.starling", "claretta.snow", "avery.mcgregor", "genny.preston", "chieko.bible", "odelia.criswell", "herb.slocum", "shaunda.castleberry" };
            List<string> ips = new List<string> { "177.71.1.205", "26.132.121.14", "70.39.111.239", "252.157.57.129", "68.57.235.228", "68.190.178.170", "26.137.54.179", "70.94.83.7", "236.9.157.68", "228.96.220.55", "97.82.121.21", "255.118.169.124", "189.48.120.236", "190.60.87.85", "64.142.90.180", "175.233.134.207", "202.35.85.142", "42.6.249.213", "3.217.95.174", "50.129.236.68", "11.150.73.69", "192.128.202.87", "128.183.86.101", "96.253.219.118", "25.75.130.68", "22.56.136.128", "99.164.27.197", "86.83.250.50", "95.21.189.36", "30.192.240.77", "71.59.161.210", "9.246.110.155", "132.126.65.53", "163.74.227.182", "94.61.22.141", "250.111.168.107", "156.236.84.254", "42.99.253.174", "203.145.14.72", "144.152.156.38", "56.250.212.63", "12.84.239.251", "224.16.250.75", "248.193.191.82", "245.125.6.18", "60.179.167.174", "30.73.190.82", "94.168.234.85", "21.174.107.0", "217.214.207.245", "164.251.85.37", "135.253.76.95", "17.139.86.127", "106.5.184.244", "127.208.10.224", "134.136.107.42", "167.23.233.210", "141.59.27.12", "71.110.23.191", "229.74.217.91", "145.87.12.195", "87.105.10.29", "73.138.170.53", "87.46.83.18", "183.240.4.53", "164.78.244.175", "1.171.94.187", "177.155.167.39", "85.183.239.62", "75.129.68.144", "209.44.123.168", "148.187.33.89", "176.160.208.217", "190.68.8.209", "175.30.173.169", "83.169.71.1", "22.66.189.144", "15.189.44.70", "85.37.6.150", "56.22.160.120", "104.133.40.19", "243.87.223.82", "79.227.87.67", "205.121.214.69", "194.167.212.105", "242.23.74.178", "191.239.130.132", "162.117.233.15", "22.47.19.46", "80.86.21.111", "123.50.36.123", "57.99.52.106", "171.224.49.81", "210.88.36.106", "22.172.30.220", "23.83.123.66", "184.107.170.156", "252.249.188.61", "16.166.138.224", "145.143.57.143", "112.202.173.149", "163.102.135.80", "141.88.75.30", "159.70.244.211", "90.83.88.135", "191.27.83.44", "13.27.136.18", "38.163.236.176", "158.206.37.66", "115.136.21.30", "132.137.124.150", "207.190.152.211", "232.71.31.122", "123.130.163.116", "56.125.201.94", "171.101.64.199", "58.18.38.66", "125.171.56.7", "68.191.120.222", "114.38.105.141", "240.11.97.247", "137.178.52.33", "101.246.79.128", "183.235.31.138", "76.181.27.61", "171.71.52.170", "209.180.149.69", "70.6.220.102", "29.29.199.99", "35.95.136.59", "98.114.241.45", "97.46.179.36", "124.174.175.62", "77.121.143.72", "235.130.234.227", "188.80.207.92", "75.237.143.41", "183.9.204.234", "79.26.135.181", "72.59.48.4", "115.80.152.15", "190.10.47.127", "62.30.177.163", "245.59.171.11", "47.23.93.126", "0.3.212.218", "247.116.120.149", "183.177.168.131", "141.148.39.209", "174.60.249.3", "64.187.255.21", "218.35.49.57", "17.147.196.118", "222.214.150.25", "136.160.247.231", "121.59.114.99", "145.165.90.195", "245.204.189.170", "37.136.177.128", "244.212.140.109", "105.14.250.252", "224.134.101.232", "108.219.219.82", "249.43.162.137", "205.58.79.252", "34.5.180.77", "201.150.255.249", "142.249.123.207", "196.229.137.81", "21.249.122.172", "165.115.27.99", "194.252.54.200", "178.167.131.32", "160.216.16.199", "60.213.184.37", "109.114.170.58", "70.155.47.29", "3.134.140.217", "235.147.43.157", "174.202.224.75", "85.163.22.231", "135.71.104.210", "5.242.228.138", "145.234.77.56", "6.197.4.156", "82.145.167.91", "245.202.145.135", "125.119.15.40", "89.243.251.54", "245.70.67.186", "146.11.177.204", "222.160.179.120", "86.123.80.252", "101.157.89.76", "143.89.33.255", "250.27.124.214", "232.113.239.173", "221.53.95.64", "228.96.250.196", "104.103.239.93" };

            Dictionary<string, string> errors = new Dictionary<string, string>
            {
                { @"Internal transfer failed with error.
Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).
 at System.Guid.GuidResult.SetFailure(ParseFailureKind failure, String failureMessageID, Object failureMessageFormatArgument, String failureArgumentName, Exception innerException)", "An exception of type 'System.Exception' occurred and was caught." },
                { @"Internal transfer failed with error.
Validation failed with status code 7 (UnableToGetCsSpDomain) for domain ****.**", "An exception of type 'System.Exception' occurred and was caught." },
                { @"Object reference not set to an instance of an object.", "An exception of type 'System.NullReferenceException' occurred and was caught." },
                { @"could not execute batch command.[SQL: SQL not available]", "An exception of type 'NHibernate.Exceptions.GenericADOException' occurred and was caught." },
                { @"Internal error happened. Please contact technical support using the following identifier: a1aa72f3-a2fa-4a12-9449-88b19357fb4b", "An exception of type 'System.ServiceModel.FaultException' occurred and was caught." },
                { @"Object reference not set to an instance of an object. ", "An exception of type 'System.NullReferenceException' occurred and was caught." },
                { "Sequence contains no matching element", "An exception of type 'System.InvalidOperationException' occurred and was caught." },
                { @"AADSTS50055: Password is expired. Trace ID: 72f6b036-54c0-4dab-a620-e85e35f30900 Correlation ID: 18a27802-a370-460a-ad57-0f522594cb3d", "Caught unhandled exception: Microsoft.IdentityModel.Clients.ActiveDirectory.AdalServiceException." },
                { @"Command syntax error
Command: StatusDomainApplication
Command body: {""application"":null}", "An exception of type 'System.Exception' occurred and was caught." },
                { "Object reference not set to an instance of an object.  ", "An exception of type 'System.NullReferenceException' occurred and was caught." },
                { "The source was not found, but some or all event logs could not be searched.Inaccessible logs: Security.", "System.Security.SecurityException" }
            };

            var messages = errors.Keys.ToArray();
            var descriptions = errors.Values.ToArray();

            var users = usernames.ToDictionary(u => u, u =>
            {
                int index = rand.Next(ips.Count);
                var name = ips[index];
                ips.RemoveAt(index);
                return name;
            });

            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    break;
                }

                int index = rand.Next(usernames.Length);
                int index2 = rand.Next(errors.Count);

                var time = GetRandomDate(DateTime.Now, DateTime.Now.AddDays(-10));

                var dotNetLog = new DotNetLog
                {
                    User = usernames[index],
                    IpAddress = users[usernames[index]],
                    Message = messages[index2],
                    Details = descriptions[index2],
                    Category = "",
                    StackTrace = "",
                    DateTime = time.AddMinutes(-rand.Next(0, 30)),
                };

                var logData = new LogData
                {
                    Source = "win2",
                    Level = LogLevel.Error,
                    DateTime = time,
                    RawLog = JsonConvert.SerializeObject(dotNetLog)
                };

                msmq.AddMessage(logData);

                Thread.Sleep(rand.Next(50, 250));
            }
        }

        public static DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var range = to - from;

            var randTimeSpan = new TimeSpan((long)(rand.NextDouble() * range.Ticks));

            return from + randTimeSpan;
        }
    }
}

