﻿using LogManager.Base.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using NLocation = LogManager.Base.Models.Location;

namespace ProcessingService.LogAugmenters
{
    class LocationAugmenter : ILogAugmenter
    {
        public async Task<NormalizedLog> AugmentAsync(NormalizedLog log)
        {
            if (!string.IsNullOrWhiteSpace(log.IpAddress))
            {
                var location = await GetLocation(log.IpAddress);
                log.Location = new NLocation(location.Lat, location.Lon);
            }

            return log;
        }

        private async Task<Location> GetLocation(string ipAddress)
        {
            var baseAddress = new Uri($"https://api.ipdata.co/{ipAddress}");

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

                using (var response = await httpClient.GetAsync(baseAddress))
                {
                    string responseData = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<Location>(responseData);
                }
            }

        }
    }

    public class Location
    {
        [JsonProperty("latitude")]
        public double Lat { get; set; }

        [JsonProperty("longitude")]
        public double Lon { get; set; }
    }
}
