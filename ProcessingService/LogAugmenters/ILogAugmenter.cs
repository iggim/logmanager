﻿using LogManager.Base.Models;
using System.Threading.Tasks;

namespace ProcessingService.LogAugmenters
{
    interface ILogAugmenter
    {
        Task<NormalizedLog> AugmentAsync(NormalizedLog log);
    }
}
