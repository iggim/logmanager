﻿using Bazinga.AspNetCore.Authentication.Basic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CollectorApi
{
    public class DbBasicCredentialVerifier : IBasicCredentialVerifier
    {
        public Task<bool> Authenticate(string username, string password)
        {
            using (var db = new UserListContext())
            {
                return db.Users.AnyAsync(
                    u => u.Username == username && u.Password == password);
            }
        }
    }

    public class UserListContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=userlist.db");

            base.OnConfiguring(optionsBuilder);
        }
    }

    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
