﻿using CollectorApi.Models;
using Microsoft.Extensions.Options;

namespace CollectorApi.Helpers
{
    public class MSMQHelper : LogManager.Base.Helpers.MSMQHelper, LogManager.Base.Helpers.IMessageQueueHelper
    {
        public MSMQHelper(IOptions<MessageQueueSettings> options)
            : base(options.Value.Path, options.Value.Label)
        {
        }
    }
}
