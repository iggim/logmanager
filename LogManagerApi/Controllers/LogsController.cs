﻿using LogManager.Base.Helpers;
using LogManager.Base.Models;
using Microsoft.AspNetCore.Mvc;

namespace CollectorApi.Controllers
{
    [Route("api/[controller]")]
    public class LogsController : Controller
    {
        private readonly IMessageQueueHelper messageQueue;

        public LogsController(IMessageQueueHelper messageQueue)
        {
            this.messageQueue = messageQueue;
        }

        // POST api/values
        [HttpPost]
        public void Add([FromBody]LogData log)
        {
            messageQueue.AddMessage(log);
        }

    }
}
