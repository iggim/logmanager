﻿namespace CollectorApi.Models
{
    public class MessageQueueSettings
    {
        public string Path { get; set; }
        public string Label { get; set; }
    }
}
