﻿using LogManager.Base.Models;
using System.Threading.Tasks;

namespace LogManager.Base.Helpers
{
    public interface ILogStorageHelper
    {
        Task StoreAsync(NormalizedLog log);
    }
}
