﻿using LogManager.Base.Models;
using System;
using System.Collections.Generic;
using System.Messaging;
using System.Threading.Tasks;

namespace LogManager.Base.Helpers
{
    public class MSMQHelper : IMessageQueueHelper
    {
        private readonly MessageQueue messageQueue;

        public MSMQHelper(string mqPath, string mqLabel)
        {
            if (!MessageQueue.Exists(mqPath))
            {
                MessageQueue.Create(mqPath);
            }

            messageQueue = new MessageQueue(mqPath)
            {
                Label = mqLabel,
                Formatter = new XmlMessageFormatter(new Type[] { typeof(LogData) })
            };
        }

        public void AddMessage(LogData log)
        {
            messageQueue.Send(log);
        }

        public async Task<LogData> GetMessageAsync()
        {
            Message msg = await Task.Factory.FromAsync<Message>(
                messageQueue.BeginReceive(),
                messageQueue.EndReceive);

            return (LogData)msg.Body;
        }

        public IEnumerable<LogData> GetMessages()
        {
            var enumerator = messageQueue.GetMessageEnumerator2();
            while (enumerator.MoveNext())
            {
                Message msg = messageQueue.ReceiveById(enumerator.Current.Id);

                yield return (LogData)msg.Body;
            }
        }
    }
}
