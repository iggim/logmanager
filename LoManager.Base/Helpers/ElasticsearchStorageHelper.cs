﻿using Elasticsearch.Net;
using LogManager.Base.Models;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LogManager.Base.Helpers
{
    public class ElasticsearchStorageHelper : ILogStorageHelper
    {
        private readonly ElasticClient client;

        public ElasticsearchStorageHelper(string url) : this(url, 1, 2)
        { }

        public ElasticsearchStorageHelper(string url, int numReplicas, int numShards)
        {
            client = CreateClient(url);

            SetupIndex(numReplicas, numShards);
        }

        private ElasticClient CreateClient(string url)
        {
            var connectionPool = new SingleNodeConnectionPool(new Uri(url));
            var connection = new HttpConnection();


            var connectionSettings = new ConnectionSettings(connectionPool, settings => new LogSerializer(settings));
            connectionSettings.DefaultIndex("logmanager");

            return new ElasticClient(connectionSettings);
        }

        private void SetupIndex(int numReplicas, int numShards)
        {
            var settings = new IndexSettings
            {
                NumberOfReplicas = numReplicas,
                NumberOfShards = numShards
            };

            var indexConfig = new IndexState
            {
                Settings = settings
            };

            if (!client.IndexExists("logmanager").Exists)
            {
                var response = client.CreateIndex("logmanager", c => c
                   .InitializeUsing(indexConfig)
                   .Mappings(ms => ms
                       .Map<NormalizedLog>(m => m.AutoMap())
                       .Map<KeyValue>(m => m.AutoMap())
                   ));
            }
        }

        public async Task StoreAsync(NormalizedLog log)
        {
            IIndexResponse response = await client.IndexAsync(log);

            if (response.Result != Result.Created)
            {
                throw new Exception($"Failed to store log, returned status: {response.Result.ToString()}");
            }
        }
    }

    public class LogSerializer : JsonNetSerializer
    {
        public LogSerializer(IConnectionSettingsValues settings) : base(settings) { }

        protected override IList<Func<Type, JsonConverter>> ContractConverters => new List<Func<Type, JsonConverter>>
        {
            t => {
                if (t == typeof(DateTime) ||
                    t == typeof(DateTime?) ||
                    t == typeof(DateTimeOffset) ||
                    t == typeof(DateTimeOffset?))
                {
                    return new  IsoDateTimeConverter
                    {
                        DateTimeFormat = "yyyy-MM-dd HH.mm.ss"
                    };
                }

                if (t.IsEnum)
                {
                    return new StringEnumConverter { CamelCaseText = true };
                }

                return null;
            }
        };
    }
}
