﻿using LogManager.Base.Models;
using System.Threading.Tasks;

namespace LogManager.Base.Helpers
{
    public interface IMessageQueueHelper
    {
        void AddMessage(LogData log);

        Task<LogData> GetMessageAsync();
    }
}
