﻿using LogManager.Base.Models;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LogManager.Base
{
    public class Listener : TraceListener
    {
        private readonly HttpClient client;
        private readonly string baseUrl;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="initializeData"></param>
        public Listener(string initializeData) : base(initializeData)
        {
            // create http client instance
            client = new HttpClient();

            // get basic auth credentials from initializeData
            var config = JsonConvert.DeserializeObject<ListenerConfig>(initializeData);
            baseUrl = config.BaseUrl;

            // set authorization header
            var byteArray = Encoding.ASCII.GetBytes($"{config.User}:{config.Password}");
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }

        private void SendMessage(
            string category,
            string description,
            string stackTrace,
            string detailedErrorDescription,
            LogLevel logCategory = LogLevel.Info)
        {
            var rawData = new DotNetLog
            {
                Message = description,
                StackTrace = stackTrace,
                Details = detailedErrorDescription,
                Category = category
            };

            var log = new LogData
            {
                DateTime = DateTime.Now,
                Level = logCategory,
                RawLog = JsonConvert.SerializeObject(rawData)
            };

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            PostLogData(log);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private async Task PostLogData(LogData log)
        {
            var result = await client.PostAsJsonAsync("", log);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                return;
            }
            else
            {
                string resultContent = await result.Content.ReadAsStringAsync();
                // write to error log
                EventLogHelper.WriteLog(resultContent);

                // TODO: preserve log in file
            }
        }

        #region OVERRIDEN METHODS
        public override void Write(string message)
        {
            StackTrace objTrace = new StackTrace(true);
            SendMessage("", message, objTrace.ToString(), "");
        }

        public override void Write(object o)
        {
            StackTrace objTrace = new StackTrace(true);
            SendMessage("", o.ToString(), objTrace.ToString(), "");
        }

        public override void Write(string message, string category)
        {
            StackTrace objTrace = new StackTrace(true);
            SendMessage(category, message, objTrace.ToString(), "");
        }

        public override void Write(object o, string category)
        {
            StackTrace objTrace = new StackTrace(true);
            SendMessage(category, o.ToString(), objTrace.ToString(), "");
        }

        public override void WriteLine(string message)
        {
            Write(message);
        }

        public override void WriteLine(object o)
        {
            Write(o.ToString());
        }

        public override void WriteLine(string message, string category)
        {
            Write((message + "\n"), category);
        }

        public override void WriteLine(object o, string category)
        {
            Write((o.ToString() + "\n"), category);
        }

        public override void Fail(string message)
        {
            StackTrace objTrace = new StackTrace(true);
            SendMessage("Fail", message, objTrace.ToString(), "", LogLevel.Error);
        }

        public override void Fail(string message, string detailMessage)
        {
            StackTrace objTrace = new StackTrace(true);
            SendMessage("Fail", message, objTrace.ToString(), detailMessage, LogLevel.Error);
        }
        #endregion OVERRIDEN METHODS
    }

    class EventLogHelper
    {
        public static void WriteLog(string log)
        {
            if (!EventLog.SourceExists("LogListener"))
            {
                // create event source if does not exists
                EventLog.CreateEventSource("LogListener", "LogManager");
            }

            // create an EventLog instance and assign its source.
            using (var eventLog = new EventLog())
            {
                eventLog.Source = "LogListener";
                eventLog.Log = "LogManager";

                eventLog.WriteEntry(log);
            }
        }
    }
}
