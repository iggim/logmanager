﻿using System;

namespace LogManager.Base.Models
{
    [Serializable]
    public class DotNetLog
    {
        public string User { get; set; }

        public string Message { get; set; }

        public string Details { get; set; }

        public string Category { get; set; }

        public string StackTrace { get; set; }

        public DateTime DateTime { get; set; }

        public string IpAddress { get; set; }

        public DotNetLog()
        {
            DateTime = DateTime.Now;
        }
    }
}
