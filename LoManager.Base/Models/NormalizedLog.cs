﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LogManager.Base.Models
{
    [ElasticsearchType(Name = "log")]
    public class NormalizedLog
    {
        [Keyword(Store = true, Index = true)]
        public string Source { get; set; }

        [Text(Store = true, Index = true, TermVector = TermVectorOption.WithPositionsOffsets)]
        public string Message { get; set; }

        [Keyword(Store = true, Index = true)]
        public LogType Type { get; set; }

        [Date(Store = true, Index = true, Format = "yyyy-MM-dd HH.mm.ss")]
        public DateTime DateGenerated { get; set; }

        [Date(Store = true, Index = false, Format = "yyyy-MM-dd HH.mm.ss")]
        public DateTime DateReceived { get; set; }

        [Keyword(Store = true, Index = true)]
        public string User { get; set; }

        [Keyword(Store = true, Index = true)]
        public string IpAddress { get; set; }

        [Keyword(Store = true, Index = true)]
        public LogLevel Level { get; set; }

        [Text(Store = true, Index = false)]
        public string RawLog { get; set; }

        [GeoPoint(Store = true)]
        public Location Location { get; set; }

        [Nested]
        public List<KeyValue> Details { get; set; }
        public NormalizedLog Result { get; set; }

        public NormalizedLog()
        {
            Type = LogType.Other;
            Details = new List<KeyValue>();
        }

        public NormalizedLog(LogData log) : this()
        {
            DateReceived = log.DateTime;
            Level = log.Level;
            Source = log.Source;
            RawLog = log.RawLog;
        }
    }

    [ElasticsearchType(Name = "keyvalue")]
    public class KeyValue
    {
        [Keyword(Store = true, Index = true)]
        public string Key { get; set; }

        [Text(Store = true, Index = true)]
        public string Value { get; set; }
    }

    public class Location
    {
        public Location(double lat, double lon)
        {
            Lat = lat;
            Lon = lon;
        }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lon")]
        public double Lon { get; set; }
    }
}
