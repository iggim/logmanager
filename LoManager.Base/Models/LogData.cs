﻿using System;

namespace LogManager.Base.Models
{
    [Serializable]
    public class LogData
    {
        public string Source { get; set; }

        public LogLevel Level { get; set; }

        public DateTime DateTime { get; set; }

        public string RawLog { get; set; }

        public LogData()
        {
            DateTime = DateTime.Now;
            Level = LogLevel.Info;
        }
    }
}
