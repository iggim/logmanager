﻿using System;

namespace LogManager.Base.Models
{
    [Serializable]
    public class ListenerConfig
    {
        public string User { get; set; }

        public string Password { get; set; }

        public string BaseUrl { get; set; }
    }
}
